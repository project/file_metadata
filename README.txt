
-------------------------------------------------------
 File Metadata Module
-------------------------------------------------------

API description

File Metadata implements a metadata storage system for files. 
It utilizes the Drupal file hooks to load, store and delete 
metadata.


File Object Data Storage

We are storing metadata at $file->metadata. Metadata is 
appended as arrays in the following format:
$file->metadata = array(MODULE_NAME => 
  array(
    IDENTFIER1 => array(
      KEY1 => VALUE1,
      KEY2 => VALUE2,    
    ),
    IDENTIFIER2 => array(
      KEY3 => VALUE3,
    ),
  )
  
); 